# -*- coding: utf-8 -*-
"""
Copyright (c) <2016> <Manuel Yguel, Strataggem R&D lab>

MIT Licence

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
"""



"""Provides an Axis-Aligned Bounding Boxes (AABB) class for numpy

AABBs are a simple ND rectangles with edges aligned on the regular axis.

An AABB contains 2 x ND vectors.
The first vector (min) represents the minimum extent.
The second vector (max) represents the maximum extent.
"""

import numpy as np

class Aabb:
    def __init__(self,dim=1,x=None):
        self.dim = dim
        if( None != x ):
            self.min = x
            self.max = x
        else:
            self.min = np.full(dim, float('+inf'))
            self.max = np.full(dim, float('-inf'))

    def empty(self):
        """Returns true if the box is empty, false otherwise
        """
        return np.array_equal( self.min, np.full(self.dim, float('+inf')) ) or np.array_equal( self.max, np.full(self.dim, float('-inf')) )

    def add_point(self,pt):
        """Extends an AABB to encompass a new point.
        """
        # compare to existing AABB and update
        self.min = np.minimum(self.min, pt)
        self.max = np.maximum(self.max, pt)

    def add(self,pt):
        """Extends an AABB to encompass a new point.
        an alias for add_point
        """
        self.add_point(pt)

    def add_points(self,pts):
        """Extends an AABB to encompass a list
        of points.
        """
        # find the minimum and maximum point values
        minimum = np.amin(pts, axis=0)
        maximum = np.amax(pts, axis=0)

        # compare to existing AABB and update
        self.min = np.minimum(self.min, minimum)
        self.max = np.maximum(self.max, maximum)

    def center(self):
        """Returns the center point of the AABB.
        """
        return (self.min + self.max)*0.5

    def sizes(self):
        """Returns a vector containing the distances
        between max and min in each dimension.
        That is the diagonal vector.
        """
        return (self.max-self.min)

    def diagonal(self):
        """Returns a vector containing the distances
        between max and min in each dimension.
        That is the diagonal vector.
        """
        return self.sizes()

    def diameter(self):
        """Returns the diameter of the bounding sphere
        encompassing the bounding box.
        """
        return np.linalg.norm( self.sizes() )

    def radius(self):
        """Returns the radius of the bounding sphere
        encompassing the bounding box.
        """
        return 0.5*self.diameter()

    def add_aabb(self,aabb):
        """Extend an AABB to encompass an other AABB.
        Alias of add_box
        """
        self.min = np.minimum(self.min,aabb.min)
        self.max = np.maximum(self.max,aabb.max)

    def add_box(self,aabb):
        """Extend an AABB to encompass an other AABB.
        Alias of add_aabb
        """
        self.add_aabb(aabb)

    def add_aabbs(self,aabbs):
        """Extend an AABB to encompass a list
        of other AABBs. Alias of add_boxes.
        """
        for b in aabbs:
            self.add_aabb(b)

    def add_boxes(self,boxes):
        """Extend an AABB to encompass a list
        of other AABBs. Alias of add_aabbs.
        """
        self.add_aabbs(boxes)

    def clamp_points(self,points):
        """Takes a list of points and modifies them to
        fit within the AABB.
        """
        return np.clip(points, a_min=self.min, a_max=self.max)

    def contains(self,pt):
        """Returns true if pt is inside the bounding box false
        otherwise.
        """
        return ( (pt >= self.min).all() and (pt <= self.max).all() )
